FROM seafileltd/seafile-mc:latest

RUN apt-get update && \
    apt-get install -y ffmpeg && \
    pip install pillow moviepy && \
    apt-get clean && \
    rm -fr /var/lib/apt/lists/* /tmp/* /var/tmp/*
